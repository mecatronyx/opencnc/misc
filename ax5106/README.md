# Beckhoff AX5106

This code is an example which handle the Beckhoff AX5106 drive.  It starts the drive
in *Position Control* mode. The position command is a sinus.

It is based on *EtherCAT master* library. **It does not work on OpenCN**.
It has to be used on a computer where the original EtherCAT master is installed.

The tests were done with the old LinuxCNC machine available in C01 (pwd: 123).

* Compilation: `g++ -lethercat -L/lib -lrt -o el2252_example ax5106_example.cpp`
* Execution: `./el2252_example`
