#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/mman.h>
#include <malloc.h>
#include <string>
#include <iostream>
#include <stdarg.h>
#include <math.h>

/****************************************************************************/

#include "ecrt.h"

/****************************************************************************/

// #define LCEC_IDN_TYPE_S 0x0000
// #define LCEC_IDN_TYPE_P 0x8000
#define LCEC_IDN(type, set, block) (type | ((set & 0x07) << 12) | (block & 0x0fff))

#define IDN_TYPE_S(set, block) LCEC_IDN(0x0000, set, block)
#define IDN_TYPE_P(set, block) LCEC_IDN(0x8000, set, block)

// Application parameters
#define FREQUENCY 1000
#define CLOCK_TO_USE CLOCK_REALTIME
// #define MEASURE_TIMING

/****************************************************************************/

#define NSEC_PER_SEC (1000000000L)
#define PERIOD_NS (NSEC_PER_SEC / FREQUENCY)
// #define PERIOD_NS 250000

#define DIFF_NS(A, B) (((B).tv_sec - (A).tv_sec) * NSEC_PER_SEC + \
                       (B).tv_nsec - (A).tv_nsec)

#define TIMESPEC2NS(T) ((uint64_t)(T).tv_sec * NSEC_PER_SEC + (T).tv_nsec)

/****************************************************************************/

// configure SYNC signals
// EtherCAT
static ec_master_t *master = NULL;
static ec_master_state_t master_state = {};

static ec_domain_t *domain1 = NULL;
static ec_domain_state_t domain1_state = {};

static ec_slave_config_t *sc = NULL;
static ec_slave_config_state_t sc_state;
static std::string slave_name = "AX5106";

/****************************************************************************/

// process data
static uint8_t *domain1_pd = NULL;

#define DrivePos 0, 0
#define Beckhoff_AX5106 0x00000002, 0x13f26012

static unsigned int counter = 0;
static unsigned int blink = 0;
static unsigned int sync_ref_counter = 1;
const struct timespec cycletime = {0, PERIOD_NS};

static double channel_scale = 0.0;
static double vel_output_scale = 0.0;

/* Master 0, Slave 0, "AX5106-0000-0012"
 * Vendor ID:       0x00000002
 * Product code:    0x13f26012
 * Revision number: 0x000c0000
 */

ec_pdo_entry_info_t ax5106_pdo_entries[] = {
    {0x0086, 0x00, 16}, /* Master control word */
    {0x002F, 0x00, 32}, /* Position command value */

    {0x0087, 0x00, 16}, /* Drive status word */
    {0x0033, 0x00, 32}, /* Position feedback 1 value */
};

ec_pdo_info_t ax5106_pdos[] = {
    {0x0018, 2, ax5106_pdo_entries + 0}, /* MDT */
    {0x0010, 2, ax5106_pdo_entries + 2}, /* AT */
};

ec_sync_info_t ax5106_syncs[] = {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 1, ax5106_pdos + 0, EC_WD_DISABLE},
    {3, EC_DIR_INPUT, 1, ax5106_pdos + 1, EC_WD_DISABLE},
    {0xff}};

// DOMAIN 1
struct AX5106_offsets
{
    unsigned int ctrl_word;
    unsigned int position_command;
    unsigned int status_word;
    unsigned int pos_feedback;
};

static AX5106_offsets ax5106_offsets;

// PDO entries of domain 1
const static ec_pdo_entry_reg_t domain1_regs[] =
    {
        {DrivePos, Beckhoff_AX5106, 0x0086, 0x00, &ax5106_offsets.ctrl_word},
        {DrivePos, Beckhoff_AX5106, 0x002F, 0x00, &ax5106_offsets.position_command},
        {DrivePos, Beckhoff_AX5106, 0x0087, 0x00, &ax5106_offsets.status_word},
        {DrivePos, Beckhoff_AX5106, 0x0033, 0x00, &ax5106_offsets.pos_feedback},
        {}};

struct idn_config_entry_t
{
    uint16_t idn;
    uint16_t size;
    uint8_t *data;
};

enum IDN_VALUE_TYPE
{
    IDN_VALUE_TYPE_U16,
    IDN_VALUE_TYPE_U32
};

idn_config_entry_t idn_config_u16(uint16_t idn, uint16_t value)
{
    idn_config_entry_t entry;
    entry.idn = idn;
    entry.data = (uint8_t *)malloc(2);
    entry.size = 2;
    EC_WRITE_U16(entry.data, value);
    return entry;
}

idn_config_entry_t idn_config_u32(uint16_t idn, uint32_t value)
{
    idn_config_entry_t entry;
    entry.idn = idn;
    entry.data = (uint8_t *)malloc(4);
    entry.size = 4;
    EC_WRITE_U32(entry.data, value);
    return entry;
}

idn_config_entry_t idn_config_u64(uint16_t idn, uint64_t value)
{
    idn_config_entry_t entry;
    entry.idn = idn;
    entry.data = (uint8_t *)malloc(8);
    entry.size = 8;
    EC_WRITE_U64(entry.data, value);
    return entry;
}

idn_config_entry_t idn_config_listu16(uint16_t idn, int N, ...)
{
    va_list args;
    va_start(args, N);
    idn_config_entry_t entry;
    entry.idn = idn;
    entry.size = N * sizeof(uint16_t) + 4; // space for data + header
    entry.data = (uint8_t *)malloc(entry.size);

    EC_WRITE_U16(entry.data, N*sizeof(uint16_t));     // actual length
    EC_WRITE_U16(entry.data + 2, N*sizeof(uint16_t)); // max length???

    for (int i = 0; i < N; i++)
    {
        uint16_t value = va_arg(args, int);
        EC_WRITE_U16(entry.data + 4 + i*2, value);
    }

    va_end(args);
    return entry;
}

idn_config_entry_t idn_config_listu32(uint16_t idn, int N, ...)
{
    va_list args;
    va_start(args, N);
    idn_config_entry_t entry;
    entry.idn = idn;
    entry.size = N * sizeof(uint32_t) + 4; // space for data + header
    entry.data = (uint8_t *)malloc(entry.size);

    EC_WRITE_U16(entry.data, N*sizeof(uint32_t));     // actual length
    EC_WRITE_U16(entry.data + 2, N*sizeof(uint32_t)); // max length???

    for (int i = 0; i < N; i++)
    {
        uint32_t value = va_arg(args, int);
        EC_WRITE_U32(entry.data + 4 + i*4, value);
    }

    va_end(args);
    return entry;
}

idn_config_entry_t idn_config_data(uint16_t idn, int N, ...)
{
    va_list args;
    va_start(args, N);
    idn_config_entry_t entry;
    entry.idn = idn;
    entry.size = N + 4; // space for data + header
    entry.data = (uint8_t *)malloc(entry.size);

    EC_WRITE_U16(entry.data, N);     // actual length
    EC_WRITE_U16(entry.data + 2, N); // max length???

    for (int i = 0; i < N; i++)
    {
        uint8_t value = va_arg(args, int);
        entry.data[4 + i] = value;
    }

    va_end(args);
    return entry;
}

idn_config_entry_t idn_config_str(uint16_t idn, const char *text)
{
    int len = strlen(text);

    idn_config_entry_t entry;
    entry.idn = idn;
    entry.size = len + 1 + 4; // string + null-termination + header
    entry.data = (uint8_t *)malloc(entry.size);

    EC_WRITE_U16(entry.data, len);     // actual length
    EC_WRITE_U16(entry.data + 2, len); // max length???
    strcpy((char*)entry.data + 4, text);
    return entry;
}


idn_config_entry_t ax5106_safeop_preop_idns[] = {
    idn_config_listu16( // configuration list of AT
        IDN_TYPE_S(0, 16), 1,
        // IDN_TYPE_S(0, 135), // drive status word
        IDN_TYPE_S(0, 51) // position feedback value
        ),

    idn_config_listu16( // configuration list of MDT
        IDN_TYPE_S(0, 24), 1,
        // IDN_TYPE_S(0, 134), // master control word
        IDN_TYPE_S(0, 47) // position command value
        ),

    // idn_config_u64(IDN_TYPE_P(0, 10), 0xffff), // feature flags
    idn_config_u16(IDN_TYPE_P(0, 304), 1),      // logger level (log errors, warnings)
    idn_config_u16(IDN_TYPE_S(0, 15), 7),        // telegram type



    // safety option
    idn_config_u16(IDN_TYPE_P(0, 2000), 1),

    // primary op mode
    idn_config_u16(IDN_TYPE_S(0, 32), 3),

    idn_config_u16(IDN_TYPE_S(0, 1), 1000), // control unit cycle time [us]
    idn_config_u16(IDN_TYPE_S(0, 2), 1000), // communication cycle time [us]

    // current ctrl cycle time [us], uint16_t
    idn_config_u16(IDN_TYPE_P(0, 2), 62),

    // velocity ctrl cycle time [us], uint16_t
    idn_config_u16(IDN_TYPE_P(0, 3), 125),

    // position ctrl cycle time [us], uint16_t
    idn_config_u16(IDN_TYPE_P(0, 4), 250),


    // nominal mains voltage (decimal 1)
    idn_config_u16(IDN_TYPE_P(0, 201), 330), // 33 V because 48/sqrt(2) = 33
    idn_config_u16(IDN_TYPE_P(0, 202), 300), // positive tolerance range
    idn_config_u16(IDN_TYPE_P(0, 203), 300), // negative tolerance range

    // power mgmt control word,
    idn_config_u16(IDN_TYPE_P(0, 204), 0b100101),

    // Static external Dclink connection
    idn_config_u16(IDN_TYPE_P(0, 214), 0x0),



    // idn_config_u16(IDN_TYPE_S(0, 7), 0),         // feedback acquisition capture point [us]


    idn_config_str(IDN_TYPE_P(0, 53), "AM3011-0B00-0000"), // configured motor type, ascii string
    idn_config_str(IDN_TYPE_P(0, 54), "AX5106"), // configured drive type, ascii string

    // MOTOR CONFIGURATION
    //--------------------

    // motor peak current, uint32_t 32 [A], decimal point 3
    idn_config_u32(IDN_TYPE_S(0, 109), 1000),

    // motor continuous stall current, uint32_t [A], decimal point 3
    idn_config_u32(IDN_TYPE_S(0, 111), 200),

    // maximum motor speed [rpm]
    idn_config_u32(IDN_TYPE_S(0, 113), 3000),
    idn_config_u16(IDN_TYPE_P(0, 51), 3), // number of pole pairs
    idn_config_u16(IDN_TYPE_P(0, 55), 102), // motor emf
    idn_config_u16(IDN_TYPE_P(0, 70), 18), // continuous stall torque
    idn_config_u16(IDN_TYPE_P(0, 57), 27500), // electrical commutation offset
    //idn_config_u16(IDN_TYPE_P(0, 58), 0), // mechanical commutation offset

    // electric model
    idn_config_listu32(IDN_TYPE_P(0, 66), 2, // decimal 2
        1820, // winding resistance (phase to phase) [Ohm]
        1250  // winding inductance (phase to phase) [mH]
    ),

    // thermal motor model
    idn_config_listu16(IDN_TYPE_P(0, 62), 4,
        240, // time constant 1 [s]
        80, // warning limit [%]
        100, // error limit [%]
        1 // error reaction, 1 = shut down on error level
    ),

    idn_config_u32(IDN_TYPE_S(0, 136), 628318), // positive acceleration limit value
    idn_config_u32(IDN_TYPE_S(0, 137), 628318), // negative acceleration limit value
    idn_config_u16(IDN_TYPE_P(0, 50), 0), // motor construction type

    // Thermal overload factor (motor winding)
    idn_config_listu16(IDN_TYPE_P(0, 68), 2,
        100, // overload factor
        0    // crc16?
    ),

    // motor warning temperature [°C] (dec 1)
    idn_config_u16(IDN_TYPE_S(0, 201), 500),
    // motor shutdown temperature [°C] (dec 1)
    idn_config_u16(IDN_TYPE_S(0, 204), 800),

    idn_config_listu32(IDN_TYPE_P(0, 71), 2,
        2,
        0
    ),

    idn_config_listu32(IDN_TYPE_P(0, 89), 6,
        // current ctrl cycle time
        IDN_TYPE_P(0, 2), // this will fill both the idn and the 16 bit rsvd
        62, // min [us]
        62, // max [us]
        // mains voltage
        IDN_TYPE_P(0, 201),
        200, // min [V.1]
        400 // max [V.1]
    ),

    idn_config_u16(IDN_TYPE_S(0, 106), 300), // current loop P-gain
    idn_config_u16(IDN_TYPE_S(0, 107), 2), // current loop I-gain

    idn_config_u16(IDN_TYPE_P(0, 52), 3000), // time limitation for peak current

    idn_config_u32(IDN_TYPE_P(0, 56), 8000), // max motor speed with max torque

    idn_config_u32(IDN_TYPE_P(0, 92), 1000), // configured channel peak current
    idn_config_u32(IDN_TYPE_P(0, 93), 200),  // configured channel current

    idn_config_u32(IDN_TYPE_S(0, 91), 143163923), // bipolar velocity limit value

    idn_config_u16(IDN_TYPE_P(0, 451), 0), // current controller settings

    idn_config_u16(IDN_TYPE_P(0, 511), 50), // velocity filter low pass time constant

    // // feedback 1 type
    // idn_config_listu16(IDN_TYPE_P(0, 150),
    //     7,
    //     0,
    //     // string(30) 48 61 72 6F 77 65 23 31 30 42 52 43 58 34 30 31 4B 31 00 00 00 00 00 00 00 00 00 00 00 00
    //     'H', 'a', 'r', 'o', 'w', 'e', '#', '1', '0', 'B', 'R', 'C', 'X', '4', '0', '1', 'K', '1',
    //     0,0,0,0,0,0,0,0,0,0,0,0, // end of string
    //     0, // feedback use
    //     0, // feedback direction

    // )
    idn_config_data(IDN_TYPE_P(0, 150), 124,
        /*0  */0x07, 0x00, 0x00, 0x00, 0x48, 0x61, 0x72, 0x6F, 0x77, 0x65, 0x23, 0x31,
        /*12 */0x30, 0x42, 0x52, 0x43, 0x58, 0x34, 0x30, 0x31, 0x4B, 0x31, 0x00, 0x00,
        /*24 */0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        /*36 */0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        /*48 */0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x04, 0x00,
        /*60 */0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x02, 0x08, 0x32, 0x00, 0x7C, 0xFC,
        /*72 */0x10, 0x27, 0x00, 0x00, 0x7E, 0x04, 0xEE, 0x02, 0xF4, 0x01, 0xE8, 0x03,
        /*84 */0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        /*96 */0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        /*108*/0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        /*120*/0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        /*132*/0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        /*144*/0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        /*156*/0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        /*168*/0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        /*180*/0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        /*192*/0x00, 0x00, 0x00, 0x00, 0xA6, 0x00, 0x4A, 0x01, 0x9B, 0x00, 0x00, 0x00,
        /*204*/0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00),

    idn_config_u32(IDN_TYPE_P(0, 152), 1), // feedback 1 gear numerator
    idn_config_u32(IDN_TYPE_P(0, 153), 1), // feedback 1 gear denominator

    idn_config_u16(IDN_TYPE_P(0, 61), 2), // motor temperature sensor type

    idn_config_u32(IDN_TYPE_S(0, 100), 16), // velocity loop P-gain
    idn_config_u16(IDN_TYPE_S(0, 101), 43), // velocity loop I-gain

    idn_config_u16(IDN_TYPE_P(0, 67), 4600), // motor winding: dielectric strength

    // motor and feedback connection check
    idn_config_listu16(IDN_TYPE_P(0, 167), 10,
        0,
        400,
        90,
        30,
        0,
        0,
        0,
        0,
        0,
        0
    ),

    // idn_config_listu16(IDN_TYPE_S(0, 267), 1, 0x6161), // password: 'aa'
{}};

/*****************************************************************************/

struct timespec timespec_add(struct timespec time1, struct timespec time2)
{
    struct timespec result;

    if ((time1.tv_nsec + time2.tv_nsec) >= NSEC_PER_SEC)
    {
        result.tv_sec = time1.tv_sec + time2.tv_sec + 1;
        result.tv_nsec = time1.tv_nsec + time2.tv_nsec - NSEC_PER_SEC;
    }
    else
    {
        result.tv_sec = time1.tv_sec + time2.tv_sec;
        result.tv_nsec = time1.tv_nsec + time2.tv_nsec;
    }

    return result;
}

/*****************************************************************************/

void check_domain_state(ec_domain_t *domain, ec_domain_state_t *last_state, const char *name)
{
    ec_domain_state_t ds;
    char wc_state_string[128];

    ecrt_domain_state(domain, &ds);

    switch (ds.wc_state)
    {
    case EC_WC_ZERO:
        snprintf(wc_state_string, 128,
                 "ZERO - No registered process data were exchanged");
        break;
    case EC_WC_INCOMPLETE:
        snprintf(wc_state_string, 128,
                 "INCOMPLETE - Some of the registered process data were exchanged");
        break;
    case EC_WC_COMPLETE:
        snprintf(wc_state_string, 128,
                 "COMPLETE - All registered process data were exchanged");
        break;
    }

    if (ds.working_counter != last_state->working_counter)
        printf("  * Domain %s working counter: %u.\n", name, ds.working_counter);
    if (ds.wc_state != last_state->wc_state)
        printf("  * Domain %s state: %s.\n", name, wc_state_string);

    *last_state = ds;
}

/*****************************************************************************/

void check_master_state(void)
{
    ec_master_state_t ms;

    ecrt_master_state(master, &ms);

    if (ms.slaves_responding != master_state.slaves_responding)
    {
        printf("%u slave(s) responding.\n", ms.slaves_responding);
    }

    if (ms.al_states != master_state.al_states)
    {
        printf("AL states:", ms.al_states);
        if ((ms.al_states & 0x1) != 0)
            printf(" INIT");
        if ((ms.al_states & 0x2) != 0)
            printf(" PREOP");
        if ((ms.al_states & 0x4) != 0)
            printf(" SAFEOP");
        if ((ms.al_states & 0x8) != 0)
            printf(" OP");
        printf("\n");
    }

    if (ms.link_up != master_state.link_up)
    {
        printf("Link is %s.\n", ms.link_up ? "up" : "down");
    }

    master_state = ms;
}

/****************************************************************************/

void check_slave_config_states(void)
{
    ec_slave_config_state_t s;

    ecrt_slave_config_state(sc, &s);

    if (s.al_state != sc_state.al_state)
    {
        printf("%s:", slave_name.c_str(), s.al_state);
        if ((s.al_state & 0x1) != 0)
            printf(" INIT");
        if ((s.al_state & 0x2) != 0)
            printf(" PREOP");
        if ((s.al_state & 0x4) != 0)
            printf(" SAFEOP");
        if ((s.al_state & 0x8) != 0)
            printf(" OP");
        printf(" state \n");
    }

    if (s.online != sc_state.online)
        printf("%s: %s.\n", slave_name.c_str(), s.online ? "online" : "offline");

    if (s.operational != sc_state.operational)
        printf("%s: %soperational.\n", slave_name.c_str(), s.operational ? "" : "Not ");

    sc_state = s;
}
/****************************************************************************/

void cyclic_task()
{
    struct timespec wakeupTime, time;
    int32_t anaIn0 = 0.0;
    uint32_t meanCount = 0;

#ifdef MEASURE_TIMING
    struct timespec startTime, endTime, lastStartTime = {};
    uint32_t period_ns = 0, exec_ns = 0, latency_ns = 0,
             latency_min_ns = 0, latency_max_ns = 0,
             period_min_ns = 0, period_max_ns = 0,
             exec_min_ns = 0, exec_max_ns = 0;
    double latency_mean_ns, period_mean_ns, exec_mean_ns;

#endif

    // get current time
    clock_gettime(CLOCK_TO_USE, &wakeupTime);

    while (1)
    {
        wakeupTime = timespec_add(wakeupTime, cycletime);
        clock_nanosleep(CLOCK_TO_USE, TIMER_ABSTIME, &wakeupTime, NULL);

#ifdef MEASURE_TIMING
        clock_gettime(CLOCK_TO_USE, &startTime);
        latency_ns = DIFF_NS(wakeupTime, startTime);
        period_ns = DIFF_NS(lastStartTime, startTime);
        exec_ns = DIFF_NS(lastStartTime, endTime);
        lastStartTime = startTime;

        meanCount++;
        latency_mean_ns += (double)latency_ns;
        period_mean_ns += (double)period_ns;
        exec_mean_ns += (double)exec_ns;

        if (latency_ns > latency_max_ns)
        {
            latency_max_ns = latency_ns;
        }
        if (latency_ns < latency_min_ns)
        {
            latency_min_ns = latency_ns;
        }
        if (period_ns > period_max_ns)
        {
            period_max_ns = period_ns;
        }
        if (period_ns < period_min_ns)
        {
            period_min_ns = period_ns;
        }
        if (exec_ns > exec_max_ns)
        {
            exec_max_ns = exec_ns;
        }
        if (exec_ns < exec_min_ns)
        {
            exec_min_ns = exec_ns;
        }
#endif

        // receive process data
        ecrt_master_receive(master);
        ecrt_domain_process(domain1);

        // check process data state (optional)
        check_domain_state(domain1, &domain1_state, "1");

        // DC CLOCK SYNC

        // write application time to master
        clock_gettime(CLOCK_TO_USE, &time);
        ecrt_master_application_time(master, TIMESPEC2NS(time));

        if (sync_ref_counter)
        {
            sync_ref_counter--;
        }
        else
        {
            sync_ref_counter = 0; // sync every cycle
        }

        ecrt_master_sync_reference_clock(master);
        ecrt_master_sync_slave_clocks(master);


        uint16_t ctrl = EC_READ_U16(domain1_pd + ax5106_offsets.ctrl_word);
        ctrl |= 1 << 14; // enable drive
        ctrl |= 1 << 15; // drive on
    ctrl |= 1 << 13; // restart ?
        ctrl ^= 1 << 10;
        EC_WRITE_U16(domain1_pd + ax5106_offsets.ctrl_word, ctrl);

        double velo_cmd_raw = 1 * channel_scale * vel_output_scale;
        if (velo_cmd_raw > (double)0x7fffffff) {
            velo_cmd_raw = (double)0x7fffffff;
        }
         if (velo_cmd_raw < (double)-0x7fffffff) {
            velo_cmd_raw = (double)-0x7fffffff;
        }

//        EC_WRITE_S32(domain1_pd + ax5106_offsets.velocity_command, (int32_t)velo_cmd_raw);
    uint16_t status = EC_READ_U16(domain1_pd + ax5106_offsets.status_word);
    static int position_command = 0;
    static double position_t = 0;
    position_command = sin(position_t)*1000000;
    //position_command = 0;
    static int resolver_ok = 0;
        static int32_t resolver = 0;
    if ((status & (0b11 << 14)) && !resolver_ok) {
        printf("drive is read to operate\n");
        resolver_ok = 1;
        resolver = EC_READ_S32(domain1_pd + ax5106_offsets.pos_feedback);
    }
    else {
        position_t += 0.004;
    }
        EC_WRITE_S32(domain1_pd + ax5106_offsets.position_command, resolver + position_command);
    {
        static int count = 1000;
        if (--count <= 0) {
            count = 1000;
            printf("resolver: %d\n", resolver);
        }
    }

        // send process data
        ecrt_domain_queue(domain1);
        ecrt_master_send(master);

#ifdef MEASURE_TIMING
        clock_gettime(CLOCK_TO_USE, &endTime);
        {
            static int count = 1000000000;
            if (--count <= 0) {
                printf("latency: (%u %u %.0f), period: (%u %u %.0f) exec: (%u %u %.0f)\n",
                    latency_min_ns, latency_max_ns, latency_mean_ns/meanCount,
                    period_min_ns, period_max_ns, period_mean_ns/meanCount,
                    exec_min_ns, exec_max_ns, exec_mean_ns
                );
                count = 1000;
            }
        }
#endif
    }
}

/****************************************************************************/

int main(int argc, char **argv)
{
    if (mlockall(MCL_CURRENT | MCL_FUTURE) == -1)
    {
        perror("mlockall failed");
        return -1;
    }

    master = ecrt_request_master(0);
    if (!master)
        return -1;

    domain1 = ecrt_master_create_domain(master);
    if (!domain1)
        return -1;

    // Create configuration for AX5106
    sc = ecrt_master_slave_config(master, DrivePos, Beckhoff_AX5106);
    if (!sc)
        return -1;


    uint8_t idn_buf[20];
    size_t result_size;
    uint16_t error_code;

    // EC_WRITE_U16(idn_buf, 1);
    // ecrt_slave_config_idn(sc, 0, IDN_TYPE_P(0, 2000), EC_AL_STATE_PREOP, idn_buf, 2);

    idn_config_entry_t* current_entry = ax5106_safeop_preop_idns;
    while (current_entry->idn != 0) {
        printf("Configuring idn: 0x%x, size: %d, data: ", current_entry->idn, current_entry->size);
        for (int i = 0; i < current_entry->size; i++) {
            printf("0x%02x ", current_entry->data[i]);
        }
        printf("\n");
        ecrt_slave_config_idn(sc, 0,
            current_entry->idn,
            EC_AL_STATE_PREOP,
            current_entry->data,
            current_entry->size
        );

        current_entry++;
    }

    // configure SYNC signals
    ecrt_slave_config_dc(sc, 0x730, 250000, 0, 750000, 0);

    printf("Configuring PDOs...\n");
    if (ecrt_slave_config_pdos(sc, EC_END, ax5106_syncs))
    {
        fprintf(stderr, "Failed to configure PDOs of AX5106.\n");
        return -1;
    }

        // Register PDOs
    printf("Configuring domains with registered PDO entries...\n");
    if (ecrt_domain_reg_pdo_entry_list(domain1, domain1_regs))
    {
        fprintf(stderr, "PDO entry registration failed for domain 1!\n");
        return -1;
    }

    ecrt_master_select_reference_clock(master, sc);

    // ecrt_master_read_idn(master, 0, 0, IDN_TYPE_P(0, 2000), idn_buf, 2, &result_size, &error_code);
    // printf("Read P-0-2000(%d, %d): %d\n", result_size, error_code, EC_READ_U16(idn_buf));

    ecrt_master_read_idn(master, 0, 0, IDN_TYPE_S(0, 45), idn_buf, 2, &result_size, &error_code);
    int32_t idn_vel_scale = EC_READ_U16(idn_buf);

    ecrt_master_read_idn(master, 0, 0, IDN_TYPE_S(0, 46), idn_buf, 2, &result_size, &error_code);
    int32_t idn_vel_exp = EC_READ_S16(idn_buf);
    channel_scale = (double)idn_vel_scale * pow(10.0, (double)idn_vel_exp);
    if (channel_scale > 0.0) {
        vel_output_scale = 60.0/channel_scale;
    } else {
        vel_output_scale = 0.0;
    }

    printf("Activating master...\n");
    if (ecrt_master_activate(master))
    {
        printf("Failed to activate\n");
        return -1;
    }

    if (!(domain1_pd = ecrt_domain_data(domain1)))
    {
        printf("Failed domain_data\n");
        return -1;
    }

    pid_t pid = getpid();
    if (setpriority(PRIO_PROCESS, pid, -19))
        fprintf(stderr, "Warning: Failed to set priority: %s\n",
                strerror(errno));
    printf("Starting cyclic function.\n");



    cyclic_task();

    return 0;
}

/****************************************************************************/
